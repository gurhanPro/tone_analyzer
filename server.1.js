const express = require("express");

const app = express();
const port = process.env.PORT || 6990;
const ToneAnalyzerV3 = require("watson-developer-cloud/tone-analyzer/v3");

const cred = {
    apikey: "-xNzHLzsKf_a75wZP3pdLbPXq1vJgA3DjFQvyU4fN2HV",
    url: "https://gateway-lon.watsonplatform.net/tone-analyzer/api",
    version: "2017-09-21",
};

app.listen(port, () => console.log(`Listening on port ${port}`));
const cus = [{ name: "ziyaad" }, { name: "homadeas" }];
app.get("/api/tones/:text", (req, res) => {
    const text2 = req.params.text;

    const toneAnalyzer = new ToneAnalyzerV3({
        version: cred.version,
        iam_apikey: cred.apikey,
        url: cred.url
    });

    const toneParams = {
        tone_input: { text: text2 },
        content_type: "application/json"
    };

    toneAnalyzer
        .tone(toneParams)
        .then(toneAnalysis => {
            res.send(JSON.stringify(toneAnalysis, null, 2));
        })
        .catch(err => {
            res.send("error:", err);
        });

});
app.get("/", (req, res) => {

    res.send({ express: "Running express backend for Tone Analzer" });
});
