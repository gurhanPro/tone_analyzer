import React from 'react';
import { Provider } from "react-redux";
import { BrowserRouter as Router, Route } from "react-router-dom";
import createHistory from "history/createBrowserHistory";
import './App.css';
import {store} from './store'
import ToneAnalyzer  from './containers/ToneAnalyzer';
const history = createHistory();

function App() {
  return (
    
    <Provider store={store}>
    <Router history={history}>
      <div className="container">
        <div className="App" style={{width:"500px" , margin:"auto"}}>
        <ToneAnalyzer />
        </div>
      </div>
    </Router>
  </Provider>
  );
}

export default App;
