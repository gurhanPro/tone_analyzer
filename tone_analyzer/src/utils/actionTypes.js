export const ActionsTypes = Object.freeze({
    // action signals for tones
    ToneRequest: "Login-Request",
    ToneSuccessfull: "Login-Successfull",
    ToneFailed: "Login-Failed",
});