import { fork, all } from 'redux-saga/effects';
import { toneWactherSaga } from './containers/ToneAnalyzer/saga'


export default function* rootsaga(){
    yield all([
        fork(toneWactherSaga)
    ]);
}