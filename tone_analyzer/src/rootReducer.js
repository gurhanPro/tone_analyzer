import tonesReducer from './containers/ToneAnalyzer/reducer'
import { combineReducers } from 'redux'

export default function createReducers(){
    return combineReducers({
        toneState: tonesReducer,
    })
}