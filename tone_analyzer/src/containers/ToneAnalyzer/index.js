import React, { Component } from "react";
import { connect } from "react-redux";
import TextField from "@material-ui/core/TextField";
import Chip from "@material-ui/core/Chip";
import Button from "@material-ui/core/Button";
import { toneRequestAcion } from './actions'
import { log } from "util";


class ToneAnalyzer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            tones: [],
            emailText: "",
        }
    }

    analyzeTone() {
        this.props.dispatch(toneRequestAcion(this.state.emailText))
    }
    loadEmailText() {
        this.setState({
            emailText: "Hi Team " 

            +" The times are difficult! Our sales have been disappointing "
            +"   for the past three quarters for our data analytics product suite."
            +"    We have a competitive data analytics product suite in the industry. "
            +"     However, we are not doing a good job at selling it, and this is really frustrating."

                + "We are missing critical sales opportunities. We cannot blame the economy for our lack of execution. Our clients need analytical tools to change their current business outcomes. In fact, it is in times such as this, our clients want to get the insights they need to turn their businesses around. It is disheartening to see that we are failing at closing deals, in such a hungry market. Let's buckle up and execute."

                + "Jennifer Baker"
                + "Sales Leader, North-East region"

        })
    }
    handleEmailTextChange(event) {
        this.setState({
            [event.target.name]: event.target.value
        })
    }


    render() {
        const currentState = this.props.toneState.tones || {};
        if (this.props.toneState.tones) {
            // console.log(currentState);
            console.log(currentState.document_tone.tones);
            console.log(currentState.sentences_tone.tones);



        }
        let tones = [];
        let sentenceTones =[]
        if (this.props.toneState.tones) {
            tones = this.props.toneState.tones.document_tone.tones
            sentenceTones = this.props.toneState.tones.sentences_tone.tones
            console.log(sentenceTones);
            

        } else {
            tones = [
                { tone_name: "waiting to load emails:", score: 0 },]
        }


        return (
            <div>
                <h1 style={{ color: "#f50057" }}>Tone Analyzer - ReactJs App</h1>
                <div>

                    {


                        tones.map(tone => (
                            <Chip label={tone.tone_name + "  :   " + tone.score} />
                        ))
                    }

                    <br />
                    <br />
                </div>
                <TextField
                    style={{ width: "100%" }}
                    id="outlined-multiline-static"
                    label="Email Loaded"
                    multiline
                    rowsMin="54"
                    defaultValue="What is Lorem Ipsum?Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum"
                    margin="normal"
                    variant="outlined"
                    name="emailText"
                    value={this.state.emailText}
                    onChange={this.handleEmailTextChange.bind(this)}

                />
                <Button
                    style={{ margin: "3px" }}
                    variant="contained"
                    color="secondary"
                    onClick={this.loadEmailText.bind(this)}
                >
                    Load email text
        </Button>

                <Button
                    style={{ margin: "8px" }}
                    variant="contained"
                    color="secondary"
                    onClick={this.analyzeTone.bind(this)}
                >
                    Analyze
        </Button>
            </div>
        );
    }
}
export default connect(state => ({
    toneState: state.toneState,
}))(ToneAnalyzer);
