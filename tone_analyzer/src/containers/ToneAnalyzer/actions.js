import { ActionsTypes } from '../../utils/actionTypes'

export function toneRequestAcion(text) {
    return {
        type: ActionsTypes.ToneRequest,
        payload: text
    }
    
}

export function toneFailedAction(error) {
    return {
        type: ActionsTypes.ToneFailed,
        payload: error
    }
    
}

export function toneSuccessAction(tones) {
    return {
        type: ActionsTypes.ToneSuccessfull,
        payload: tones
    }
}