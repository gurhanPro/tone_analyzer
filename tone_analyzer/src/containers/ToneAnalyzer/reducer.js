import { ActionsTypes } from '../../utils/actionTypes'

const initialState = {
    emailText: null,
    tones: null,
    loader: false,
    error: null
}

export default function tonesReducer(state = initialState, action) {
    switch (action.type) {
        case ActionsTypes.ToneRequest:
            return {
                ...state,
                loader: true
            };
        case ActionsTypes.ToneFailed:
            return {
                ...state,
                error: action.payload
            };
        case ActionsTypes.ToneSuccessfull:
            return {
                ...state,
                tones: action.payload
            }
        default:
            return state;
    }

}