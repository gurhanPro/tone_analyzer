import { take, call, put } from 'redux-saga/effects';
import { ActionsTypes} from '../../utils/actionTypes'
import { getTones } from './request'
import * as actions from './actions'

export function* toneWactherSaga(){
        console.log("started toneWatcher saga");

    // an infintie loop that waits for ToneRequest action to be triggered
    for(;;){
        const { payload } = yield take(ActionsTypes.ToneRequest)
        try {
            const response = yield call(getTones, payload)
            yield put(actions.toneSuccessAction(response.data))
            
        } catch (error) {
            // see usefull error object
            console.log(error);
            
            yield put(actions.toneFailedAction(error.response))
        }
    }
}