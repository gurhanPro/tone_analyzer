'use strict';

const {google} = require('googleapis');
const sampleClient = require('./sampleclient');

const gmail = google.gmail({
  version: 'v1',
  auth: sampleClient.oAuth2Client,
});

async function runSample() {
  const res = await gmail.users.messages.list({userId: 'me'});
  console.log(res.data);
  return res.data;
}

if (module === require.main) {
  const scopes = ['https://www.googleapis.com/auth/gmail.readonly'];
  sampleClient
    .authenticate(scopes)
    .then(runSample)
    .catch(console.error);
}

module.exports = {
  runSample,
  client: sampleClient.oAuth2Client,
};
